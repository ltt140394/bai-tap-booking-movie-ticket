import React, { Component } from 'react'
import { connect } from 'react-redux'
import { actionCreator } from './constants/action'

class PickedList extends Component {
    render() {
        const handleTinhTongTien = (danhSachGheDaDat) => {
            let total = 0;

            danhSachGheDaDat.forEach((ghe) => {
                total += ghe.gia;
            });

            return new Intl.NumberFormat('vn-VN').format(total);
        }

        return (
            <div className="p-5">
                <h1 className="pickedList__title">DANH SÁCH GHẾ BẠN CHỌN</h1>
                <div className="d-flex align-items-end">
                    <div className="gheDuocChonPickedList d-inline-block mr-2"></div>
                    <span style={{ color: "white", fontWeight: 'bold', fontSize: "20px" }} >Ghế đã đặt</span>
                </div>
                <div className="d-flex align-items-end my-2">
                    <div className="gheDangChonPickedList d-inline-block mr-2"></div>
                    <span style={{ color: "white", fontWeight: 'bold', fontSize: "20px" }} >Ghế đang chọn</span>
                </div>
                <div className="d-flex align-items-end">
                    <div className="gheChuaChonPickedList d-inline-block mr-2"></div>
                    <span style={{ color: "white", fontWeight: 'bold', fontSize: "20px" }} >Ghế chưa đặt</span>
                </div>

                {
                    this.props.danhSachGheDaDat.length > 0 &&
                    <div id="bookingTicket">
                        <table className="table table-bordered mt-5" style={{ fontSize: "20px" }}>
                            <thead className="text-left text-white font-weight-bold">
                                <tr>
                                    <td>Số ghế</td>
                                    <td>Giá (VNĐ)</td>
                                    <td className="text-center">Hủy</td>
                                </tr>
                            </thead>
                            <tbody className="text-left text-warning font-weight-bold">
                                {this.props.danhSachGheDaDat.map((item, index) => {
                                    return <tr key={index}>
                                        <td>{item.soGhe}</td>
                                        <td>{new Intl.NumberFormat('vn-VN').format(item.gia)}</td>
                                        <td onClick={() => {
                                            this.props.handleXoaGhe(item.soGhe)
                                        }} className="text-danger text-center" style={{ cursor: "pointer" }}>X</td>
                                    </tr>
                                })}
                                <tr>
                                    <td className="text-white">Tổng tiền</td>
                                    <td className="text-warning text-center" colSpan={2}>
                                        {handleTinhTongTien(this.props.danhSachGheDaDat)}
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <button className="btn btn-info w-50" onClick={() => {
                            this.props.handleXacNhanDatVe(this.props.danhSachGheDaDat);
                        }}>Xác nhận đặt vé</button>
                    </div>
                }

                <div id="confirmTicket" className="text-white font-weight-bold text-left" style={{ fontSize: "22px" }}>
                    <h1 className="text-center display-4 mt-5 mb-2">Đặt vé thành công</h1>
                    <p>Chỗ ngồi :
                        {this.props.danhSachGheDaDat.map((item, index) => {
                            return <span key={index} className="text-warning mx-1">{item.soGhe}</span>
                        })}
                    </p>
                    <p>Tổng tiền: <span className="text-warning">{handleTinhTongTien(this.props.danhSachGheDaDat)} VNĐ</span></p>
                    <p>Mã vé: <span className="text-warning">E6B3AGGH-0HA3</span></p>
                </div>
            </div>
        )
    }
}


let mapStateToProps = (state) => {
    return {
        danhSachGheDaDat: state.seatReducer.danhSachGheDaDat,
    }
}

let mapDispatchToProps = (dispatch) => {
    return {
        handleXoaGhe: (soGhe) => {
            dispatch(actionCreator.xoaGhe(soGhe));
        },

        handleXacNhanDatVe: (danhSachGheDaDat) => {
            dispatch(actionCreator.xacNhanDatVe(danhSachGheDaDat));
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(PickedList);
