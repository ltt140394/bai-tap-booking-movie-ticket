import React, { Component } from 'react'
import { connect } from 'react-redux'
import { actionCreator } from './constants/action'

class SeatItem extends Component {
    render() {
        return (
            <div style={{ display: 'flex', justifyContent: "center", marginBottom: "15px", position: "relative" }}>
                <span className="firstChar">{this.props.seatItem.hang}</span>
                <div className="row w-100 gx-5">
                    {this.props.seatItem.danhSachGhe.map((item, index) => {
                        if (this.props.seatItemIndex == 0) {
                            return <div className="col-1" key={index + item.soGhe}>
                                <div className="rowNumber">{item.soGhe}</div>
                            </div>
                        } else if (item.daDat) {
                            return <div className="col-1" key={index + item.soGhe}>
                                <div className="gheDuocChon">{item.soGhe}</div>
                            </div>
                        } else {
                            return <div className="col-1" key={index + item.soGhe}>
                                <div onClick={() => {
                                    this.props.handleChonGhe(item);
                                }} className="gheChuaChon" id={item.soGhe}>{item.soGhe}</div>
                            </div>
                        }
                    })}
                </div>
            </div>
        )
    }
}


let mapDispatchToProps = (dispatch) => {
    return {
        handleChonGhe: (ghe) => {
            dispatch(actionCreator.chonGhe(ghe));
        }
    }
}

export default connect(null, mapDispatchToProps)(SeatItem);
