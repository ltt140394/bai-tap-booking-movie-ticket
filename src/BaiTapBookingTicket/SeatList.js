import React, { Component } from 'react'
import { connect } from 'react-redux'
import SeatItem from './SeatItem'

class SeatList extends Component {
    render() {
        return (
            <div className="my-3 mx-auto" style={{ width: "90%" }}>
                {this.props.seatList.map((item, index) => {
                    return <SeatItem
                        key={index + item.hang}
                        seatItem={item}
                        seatItemIndex={index}
                    />
                })}
            </div>
        )
    }
}

let mapStateToProps = (state) => {
    return {
        seatList: state.seatReducer.seatList,
    }
}

export default connect(mapStateToProps)(SeatList);


