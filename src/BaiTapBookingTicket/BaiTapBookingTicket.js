import React, { Component } from 'react'
import './BaiTapBookingTicket.css'
import ScreenMovie from './ScreenMovie'
import SeatList from './SeatList'
import PickedList from './PickedList'

export default class BaiTapBookingTicket extends Component {
    render() {
        return (
            <div className="bookingMovie">
                <div className="wrap__bookingMovie">
                    <div className="row py-4">
                        <div className="col-7 text-center px-5">
                            <h1 className="booking__title">ĐẶT VÉ XEM PHIM</h1>
                            <ScreenMovie />
                            <SeatList />
                        </div>
                        <div className="col-5">
                            <PickedList />
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}



