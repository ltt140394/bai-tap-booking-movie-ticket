import { combineReducers } from "redux";
import { seatReducer } from "./seatReducer";

export const rootReducer = combineReducers({
    seatReducer,
});