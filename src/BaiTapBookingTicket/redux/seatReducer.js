import seatData from "../danhSachGhe.json"
import { CHON_GHE, XAC_NHAN_DAT_VE, XOA_GHE } from "../constants/constants";

let initialState = {
    seatList: seatData,
    danhSachGheDaDat: [],
}

export const seatReducer = (state = initialState, { type, payload }) => {
    switch (type) {
        case CHON_GHE: {
            let cloneDanhSachGheDaDat = [...state.danhSachGheDaDat];

            let index = cloneDanhSachGheDaDat.findIndex((item) => {
                return item.soGhe == payload.soGhe;
            });

            if (index == -1) {
                let gheDaDat = { ...payload };
                cloneDanhSachGheDaDat.push(gheDaDat);
                state.danhSachGheDaDat = cloneDanhSachGheDaDat;
                document.getElementById(payload.soGhe).setAttribute("class", "gheDangChon");
            }

            return { ...state };
        }

        case XOA_GHE: {
            let cloneDanhSachGheDaDat = [...state.danhSachGheDaDat];

            let index = cloneDanhSachGheDaDat.findIndex((item) => {
                return item.soGhe == payload;
            });

            cloneDanhSachGheDaDat.splice(index, 1);
            state.danhSachGheDaDat = cloneDanhSachGheDaDat;
            document.getElementById(payload).setAttribute("class", "gheChuaChon");

            return { ...state };
        }

        case XAC_NHAN_DAT_VE: {
            if (payload.length > 0) {
                payload.forEach((ghe) => {
                    document.getElementById(ghe.soGhe).setAttribute("class", "gheDuocChon");
                    ghe.daDat = true;
                });

                document.getElementById("bookingTicket").style.display = "none";
                document.getElementById("confirmTicket").style.display = "block";
            }
        }

        default: return state;
    }
}

