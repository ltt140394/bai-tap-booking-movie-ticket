import { CHON_GHE, XAC_NHAN_DAT_VE, XOA_GHE } from "./constants"

export const actionCreator = {
    chonGhe: (index) => {
        return {
            type: CHON_GHE,
            payload: index,
        }
    },

    xoaGhe: (index) => {
        return {
            type: XOA_GHE,
            payload: index,
        }
    },

    xacNhanDatVe: (index) => {
        return {
            type: XAC_NHAN_DAT_VE,
            payload: index,
        }
    }
}